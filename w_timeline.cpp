#include <QtGui> 
#include <QXmlSimpleReader>

#include "w_timeline.h"
#include "timelinehandler.h"

w_Timeline::w_Timeline(QString usr, QString pass, QWidget *parent) : QDialog(parent)
{
    setupUi(this);
    le_message->setFocus();

    timeline = new Timeline();
    tv_timeline->setModel(timeline);

    user = usr;
    password = pass;

    //Load the timeline
    refresh();

    // Send message button (Squawk!)
    QObject::connect(pb_update, SIGNAL(clicked()), this, SLOT(sendUpdate()));

    //Refresh the timeline ever so often...
    timer = new QTimer(this);
    QObject::connect(timer, SIGNAL(timeout()), this, SLOT(refresh()));
    //30 seconds
    timer->start(30000);

}

void w_Timeline::sendUpdate()
{
    pb_update->setEnabled(false);
    le_message->setEnabled(false);

    TwitterSocket *tmp = new TwitterSocket(user,password);
    QObject::connect( tmp, SIGNAL( result(bool,QString,QString) ), this, SLOT( updateResult(bool, QString) ) );
    tmp->update(le_message->text());
}

void w_Timeline::updateResult(bool error, QString errorstring)
{
    if (!error)
    {
      le_message->setEnabled(true);
      pb_update->setEnabled(true);
      le_message->clear();
      refresh();
    }
    else
      qDebug() << "An error occured in updateResult: " << errorstring;
}

void w_Timeline::refresh()
{
    TwitterSocket *tmp = new TwitterSocket(user,password);
    QObject::connect( tmp, SIGNAL( result(bool,QString,QString) ), this, SLOT( refreshResult(bool, QString, QString) ) );
    tmp->timeline();
}

void w_Timeline::refreshResult(bool error, QString errorstring, QString contents)
{
    if (!error)
    {
      QXmlSimpleReader xmlReader;
      QXmlInputSource *source = new QXmlInputSource();
      source->setData(contents);
      
      handler = new TimelineHandler;
      xmlReader.setContentHandler(handler);
      xmlReader.setErrorHandler(handler);

      QObject::connect(handler,SIGNAL(result(QStandardItem *)),this, SLOT(updateTimeline(QStandardItem *)));
      
      xmlReader.parse(source);
    }
    else
       qDebug() << "An error occured in refreshResult: " << errorstring;

}

void w_Timeline::updateTimeline(QStandardItem *item)
{
    if (!timeline->hasItem(item))
      timeline->appendRow(item);
}
