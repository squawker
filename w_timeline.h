#ifndef W_TIMELINE_H
#define W_TIMELINE_H
 
#include "ui_w_timeline.h"
#include "twittersocket.h"
#include "timelinehandler.h"
#include "timeline.h"
 
class w_Timeline : public QDialog, private Ui::w_timeline
{
    Q_OBJECT
 
public:
    w_Timeline(QString user, QString password, QWidget *parent = 0);
 
private slots:
    void sendUpdate();
    void refresh();
    void refreshResult(bool error, QString errorstring, QString contents);
    void updateResult(bool error, QString errorstring);
    void updateTimeline(QStandardItem *item);

private:
    QString user;
    QString password;
    QTimer *timer;
    TimelineHandler *handler;
    Timeline *timeline;
};
 
 
#endif
