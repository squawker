#include "timelinehandler.h"
#include <QDebug>
#include <QMessageBox>
#include <QDateTime>

TimelineHandler::TimelineHandler()
{
    status = new QStandardItem();
    inUserTag = false;
}

bool TimelineHandler::startElement(const QString & /* namespaceURI */,
                               const QString &localName,
                               const QString &,//qName
                               const QXmlAttributes &) //Attributes
{
    if (localName == "status")
      status = new QStandardItem();
    if (localName == "user")
      inUserTag = true;

//    qDebug() << "Startelement " << localName ;
    currentText.clear();
    return true;
}

bool TimelineHandler::endElement(const QString & /* namespaceURI */,
                             const QString &localName,
                             const QString &) //qName
{
    if (localName == "status")
      emit result(status);

   if (inUserTag)
    {
      if (localName == "user")
        inUserTag = false;
      if (localName == "id")
        status->setData( currentText.toInt(), UserId);
      if (localName == "name")
        status->setData( currentText, Name);
      if (localName == "screen_name")
        status->setData( currentText, Screen_name);
      if (localName == "location")
        status->setData( currentText, Location);
      if (localName == "description")
        status->setData( currentText, Description);
      if (localName == "profile_image_url")
        status->setData( currentText, Profile_image_url);
      if (localName == "url")
        status->setData( currentText, Url);
    }
    else
    {
      if (localName == "created_at")
        status->setData( QDateTime::fromString(currentText, "ddd MMM dd hh:mm:ss +0000 yyyy"), Created_at);
      if (localName == "text")
        status->setData( currentText, Text);
      if (localName == "id")
        status->setData( currentText.toInt(), Id);
    }

  //  qDebug() << "EndElement " << qName << "and " << currentText;
    return true;
}

bool TimelineHandler::fatalError(const QXmlParseException &) //exception
{
    qDebug() << "Fatal XML error occured";
    return false;
}

QString TimelineHandler::errorString() const
{
    return errorStr;
}

bool TimelineHandler::characters(const QString &str)
{
    currentText += str;
    return true;
}
