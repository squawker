#ifndef W_CONNECT_H
#define W_CONNECT_H
 
#include "ui_w_connect.h"
#include "w_timeline.h"
#include "twittersocket.h" 
 
class w_Connect : public QDialog, private Ui::w_connect
{
    Q_OBJECT
 
public:
    w_Connect(QWidget *parent = 0);
 
public slots:
    void connect();

private slots:
    void status( bool error, QString errorString);
private:
    TwitterSocket *twittersocket;
    w_Timeline *w_timeline;    
};
 
#endif
