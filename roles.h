#ifndef STATUS_H
#define STATUS_H

enum Roles {
    Profile_image_url = Qt::DecorationRole,
    Created_at = Qt::UserRole + 1,
    Id,
    Text,
    UserId,
    Name,
    Screen_name,
    Location,
    Description,
    Url
};

#endif

