#include "timeline.h"
#include <QDebug>
#include "roles.h"

Timeline::Timeline(QObject *parent) : QStandardItemModel(parent)
{
}

bool Timeline::hasItem(QStandardItem *item)
{
    // Have to add this extra check, to avoid the
    // match when we have no items.
    if (rowCount() == 0)
      return false;

    QModelIndexList tmp = match(index(0,0), Id, item->data(Id));

    // Has the item
    if (tmp.size() > 0)
      return true;

    // Dont have the item.
    return false;
}
