#include <QDebug>

#include "twittersocket.h"

TwitterSocket::TwitterSocket(QString user, QString pass, QObject *parent) : QHttp(parent)
{
    setHost("twitter.com");
    setUser(user, pass);
    QObject::connect( this, SIGNAL( done(bool) ), this, SLOT(done( bool) ) ); 
}

void TwitterSocket::accountValid()
{
    get(QLatin1String("/statuses/friends_timeline.xml"));
}

void TwitterSocket::update(QString message)
{
    QHttpRequestHeader header("POST", "/statuses/update.xml");
    header.setValue("Host", "twitter.com");
    
    // Twitterrific manages to get its name on the page using the source
    // tag, but it doesn't seem to work for us :/
    const QString msg("source=Squawker&status="+message);
    request(header, msg.toAscii());
}

void TwitterSocket::timeline()
{
    get(QLatin1String("/statuses/friends_timeline.xml"));
}

void TwitterSocket::done( bool error)
{
    QString results = readAll();
    
    // Hack to work around bug in Qt4.2 basic auth. Fixed in 4.3
    // (It doesn't report an error when incorrect credentials are supplied)
    if ( results == "Could not authenticate you")
      emit result(true, QLatin1String("Authentication denied"), NULL ); 
    else
      emit result(error, errorString(), results);
  
    deleteLater();
}
