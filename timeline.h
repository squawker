#ifndef TIMELINE_H
#define TIMELINE_H

#include <QStandardItemModel>

class Timeline : public QStandardItemModel
{
public:
  Timeline(QObject * parent = 0);
  bool hasItem(QStandardItem *item);
};

#endif

