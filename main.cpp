#include <QtGui>
#include <QHttp>

#include "w_connect.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    
    w_Connect *connectWindow = new w_Connect();
    connectWindow->show();
    
    return app.exec();
}
