#include <QtGui>

#include "w_connect.h"
#include "twittersocket.h"

w_Connect::w_Connect(QWidget *parent) : QDialog(parent)
{
    setupUi(this);

    le_user->setFocus();

    QObject::connect(pb_connect, SIGNAL(clicked()), this, SLOT(connect()));
}

void w_Connect::connect()
{
    pb_connect->setEnabled(false);
    le_password->setEnabled(false);
    le_user->setEnabled(false);
    pb_connect->setText(QLatin1String("Connecting"));

    twittersocket = new TwitterSocket(le_user->text(), le_password->text());
    QObject::connect( twittersocket, SIGNAL( result( bool, QString, QString) ), this, SLOT( status( bool, QString) ) );
    twittersocket->accountValid();
}

void w_Connect::status( bool error, QString errorString )
{
    if (error == false)
    {
        w_timeline = new w_Timeline(le_user->text(), le_password->text(), this->parentWidget());
        w_timeline->show();
        deleteLater();
    }
    else
    {
        QMessageBox::critical(this, QLatin1String("Uh-oh."),
                              "<b>An error occured:</b><br>" + errorString,
                              QMessageBox::Ok);

        pb_connect->setText(QLatin1String("Connect"));
        pb_connect->setEnabled(true);
        le_password->setEnabled(true);
        le_user->setEnabled(true);

        le_password->clear();

        qDebug() << "error in w_Connect::status:" << errorString;
    }
}
