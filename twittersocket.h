#ifndef W_TWITTERSOCKET_H
#define W_TWITTERSOCKET_H

#include <QHttp>

class TwitterSocket : public QHttp
{
    Q_OBJECT

public:
    TwitterSocket(QString user, QString pass, QObject *parent=0);

    void timeline();
    void accountValid();
    void update(QString message);

signals:
    void result( bool error, QString errorString, QString contents);
 
private:
    QHttp *http;
    
private slots:
    void done(bool error); 
};


#endif
