#ifndef TIMELINEHANDLER_H
#define TIMELINEHANDLER_H

#include <QXmlDefaultHandler>
#include <QStandardItem>

#include "roles.h"

class TimelineHandler : public QObject, public QXmlDefaultHandler
{
  Q_OBJECT

public:
    TimelineHandler();

    bool startElement(const QString &namespaceURI, const QString &localName,
                      const QString &qName, const QXmlAttributes &attributes);
    bool endElement(const QString &namespaceURI, const QString &localName,
                    const QString &qName);
    bool fatalError(const QXmlParseException &exception);
    bool characters(const QString &characters);
    QString errorString() const;

private:
    bool inUserTag;
    QString currentText;
    QString errorStr;
    QStandardItem *status;

signals:
    void result(QStandardItem *item);
};

#endif
